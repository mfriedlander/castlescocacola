﻿namespace CastlesCocaCola
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.IO.Ports;
    using System.Text;

    class Program
    {
        static SerialPort sp;

        const byte STX = 2;
        const byte ETX = 3;
        const byte MTI = 0;

        static void Main(string[] args)
        {
            string[] ports = SerialPort.GetPortNames();
            string portstring = "None";
            if(ports.Any())
            {
                portstring = String.Join(", ", ports);
            }
            Console.WriteLine($"The Following Serial Ports were Found: {portstring}");
            if(ports.Any())
            {
                string port = string.Empty;
                if(ports.Count() > 1)
                {
                    bool invalidPort = true;
                    do
                    {
                        Console.WriteLine("Enter the com port of the castles reader (ex COM1) and press Enter:");
                        string portEntered = Console.ReadLine();
                        if (ports.Contains(portEntered.ToUpper()))
                        {
                            port = portEntered;
                            invalidPort = false;
                        }
                    } while (invalidPort);
                }
                else
                {
                    port = ports[0];
                }


                if (!string.IsNullOrEmpty(port))
                {
                    sp = new SerialPort(port);
                    sp.Open();
                    sp.DataReceived += Sp_DataReceived;

                    ConsoleKeyInfo keyPress = new ConsoleKeyInfo();
                    do
                    {
                        // Message Header = DID SID MTI LenMSB LenLSB
                        byte DestinationID = 3; // terminal/pin
                        byte SourceID = 2; // POS/kiosk
                        
                        byte MessageInfoLengthMSB = 0;
                        byte MessageInfoLengthLSB = 0;
                        // Message Info = Type ID Param1 Param2
                        byte MessageType = 1; // Command
                        byte MessageId = 210; // D2
                        byte Parameter1 = 0;
                        byte Parameter2 = 0;
                        byte MessageDataLengthMSB = 0;
                        byte MessageDataLengthLSB = 0;

                        byte[] messageHeader;
                        byte[] messageInfo;
                        byte[] stx;
                        byte[] lrc;
                        IEnumerable<byte> message;
                        byte[] messageData;
                        byte[] message_arr;

                        switch (keyPress.Key)
                        {
                            case ConsoleKey.D0:
                                Console.WriteLine("SaleTransaction");
                                Console.WriteLine("Type in the dollar amount of the sale");
                                
                                
                                
                                string dollarAmount = Console.ReadLine();
                                int dollarint = 0;
                                if(Int32.TryParse(dollarAmount, out dollarint))
                                {
                                    dollarint *= 100;
                                }
                                byte[] tempDollarBytes = BitConverter.GetBytes(dollarint);
                                if(!BitConverter.IsLittleEndian)
                                {
                                    tempDollarBytes.Reverse();
                                }

                                messageData = new byte[13];
                                Array.Copy(tempDollarBytes, 0, messageData, 6 - tempDollarBytes.Length, tempDollarBytes.Length);
                                messageData[6] = (byte)(DateTime.Now.Year % 100);
                                messageData[7] = (byte)DateTime.Now.Month;
                                messageData[8] = (byte)DateTime.Now.Day;
                                messageData[9] = (byte)(DateTime.Now.Hour);
                                messageData[10] = (byte)DateTime.Now.Minute;
                                messageData[11] = (byte)DateTime.Now.Second;
                                messageData[12] = ETX;

                                // build Message Header {Message Header} = DID SID MTI LENGTH
                                DestinationID = 3; // terminal/pin
                                SourceID = 2; // POS/kiosk
                                MessageInfoLengthLSB = 6; // std

                                messageHeader = new byte[5] { DestinationID, SourceID, MTI, MessageInfoLengthMSB, MessageInfoLengthLSB };
                                
                                // build Message {Message Info} Type
                                MessageType = 1; // Command
                                MessageId = 210; // D2
                                Parameter1 = 255;
                                Parameter2 = 0;
                                MessageDataLengthMSB = 0;
                                MessageDataLengthLSB = 14;

                                messageInfo = new byte[6] { MessageType, MessageId, Parameter1, Parameter2, MessageDataLengthMSB, MessageDataLengthLSB };

                                message = messageHeader.Concat(messageInfo).Concat(messageData);

                                stx = new byte[1] { STX };
                                lrc = new byte[1] { calculateLRC(message.ToArray()) };
                                message = (stx.Concat(message).Concat(lrc)).ToArray();
                                message_arr = message.ToArray();

                                PrintByteArray(message_arr, "Castles Device Message: ");
                                sp.Write(message_arr, 0, message_arr.Length);
                                break;
                            case ConsoleKey.D1:
                                Console.WriteLine("Sending Reset");

                                // build Message Header {Message Header} = DID SID MTI LENGTH
                                DestinationID = 3; // terminal/pin
                                SourceID = 2; // POS/kiosk
                                MessageInfoLengthLSB = 6; // std
                                // build Message Header {Message Header} = DID SID MTI LENGTH
                                messageHeader = new byte[5] { DestinationID, SourceID, MTI, MessageInfoLengthMSB, MessageInfoLengthLSB };

                                // build Message {Message Info} Type
                                MessageType = 1; // Command
                                MessageId = 210; // D2
                                Parameter1 = 255;
                                Parameter2 = 0;
                                MessageDataLengthMSB = 0;
                                MessageDataLengthLSB = 14;

                                messageInfo = new byte[6] { MessageType, MessageId, Parameter1, Parameter2, MessageDataLengthMSB, MessageDataLengthLSB };

                                message = messageHeader.Concat(messageInfo);

                                stx = new byte[1] { STX };
                                lrc = new byte[1] { calculateLRC(message.ToArray()) };
                                message = (stx.Concat(message).Concat(lrc)).ToArray();
                                message_arr = message.ToArray();

                                Console.WriteLine(Encoding.ASCII.GetString(message_arr));
                                sp.Write(message_arr, 0, message_arr.Length);
                                break;
                            case ConsoleKey.D2:
                                break;
                            case ConsoleKey.D3:
                                break;
                            default:
                                Console.Clear();
                                Console.WriteLine("Select from the Menu below:");
                                Console.WriteLine("0: SaleTransaction");
                                Console.WriteLine("1: Reset Device");
                                Console.WriteLine();
                                Console.WriteLine("Press Esc to Exit Program");
                                break;
                        }
                        keyPress = Console.ReadKey(true);
                    } while (keyPress.Key != ConsoleKey.Escape);
                }
            }
        }

        private static void Sp_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                if (sp.BytesToRead > 0)
                {
                    byte[] message = new byte[sp.BytesToRead];
                    sp.Read(message, 0, sp.BytesToRead);
                    PrintByteArray(message, "Castles Device Response: ");
                    /*string msg = Encoding.ASCII.GetString(message);
                    Console.WriteLine($"Castles Device Response: {msg}");*/
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An unhandled exception has occurred with the Castles device while receiving data");
            }
        }

        public static void PrintByteArray(byte[] bytes, string prefix)
        {
            var sb = new StringBuilder(prefix + "{ ");
            List<string> byteStrings = new List<string>();
            foreach (var b in bytes)
            {
                byteStrings.Add(b.ToString());
            }
            sb.Append(string.Join(", ", byteStrings));
            sb.Append("}");
            Console.WriteLine(sb.ToString());
        }

        public static byte calculateLRC(byte[] bytes)
        {
            byte LRC = 0;
            for (int i = 0; i < bytes.Length; i++)
            {
                LRC ^= bytes[i];
            }
            return LRC;
        }
    }
}
